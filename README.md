# Remove audiobooks from Spotify liked songs playlist

## Setup
### R environment
set up the R environment:
```
install.packages("renv")
renv::activate() # activates r environment
renv::restore() # installs packages 
```
if this doesn't work, deactivate the R environment (`renv::deactivate()`) and install the packages listed in `drop_audiobooks.Rmd` manually. 

### Spotify
Follow the instructions in the [spotifyr](https://github.com/charlie86/spotifyr) README. Store the client id and the client secret in a file `.Renviron` (see `.Renviron_template`). This is loaded at the beginning of `drop_audiobooks.Rmd`.

## Execute
**Manually** step through the `drop_audiobooks.Rmd`. 

:warning: do not knit it as it includes manual steps (R chunk `select-n-audiobooks`) to determine how many albums need to be removed from "liked songs"!! :warning: 

## Caveats
Not tested whether there could be API limit restrictions. With ~1500 liked tracks I did not run into any problems, but could be different for larger "liked songs" playlists.
