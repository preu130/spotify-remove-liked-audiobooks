---
title: "Remove Audiobooks from Spotify Liked Songs"
author: "Frie Preu"
date: "8/11/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
readRenviron(".Renviron")
library(spotifyr)
library(dplyr)
library(purrr)
library(jsonlite)
library(httr)
library(usethis)
library(readr)
library(glue)
```
```{r}
access_token <- spotifyr::get_spotify_access_token()
```


```{r}
# get total number of saved tracks and calculate the offsets (can only get 50 tracks with a call)
meta <- spotifyr::get_my_saved_tracks(limit = 50, offset = 0, include_meta_info = TRUE)
total <- meta$total
offsets <- seq(0, total + 50, 50)
offsets
```

```{r}
# define function to get 50 saved tracks depending on offset 
get_chunk <- function(offset) {
  new <- spotifyr::get_my_saved_tracks(limit = 50, offset = offset, include_meta_info = FALSE)
  usethis::ui_done(glue::glue("got from offset: {offset}"))
  return(new)
}

# map over offsets, bind to dataframe 
all_tracks <- purrr::map_dfr(offsets, get_chunk)
```

```{r}
# write to disk
readr::write_rds(all_tracks, "saved_tracks.rds")
all_tracks <- readr::read_rds("saved_tracks.rds")
```

```{r}
# group by album, sort by duration 
all_tracks_by_album <- all_tracks %>% 
  group_by(track.album.id, track.album.name) %>% 
  summarize(total_duration_album =  sum(track.duration_ms)) %>% 
  arrange(desc(total_duration_album))

# determine what are the audiobooks by looking at the data
# the longest albums should be the audiobooks 
all_tracks_by_album 
  
```

```{r select-n-audiobooks}
# select the audiobooks / the n longest albums and extract the ids
audiobooks_id <- all_tracks_by_album %>% 
  head(3) %>% # from the manual investigation, i had three audiobooks
  pull(track.album.id)
audiobooks_id
```


```{r}
# filter tracks belonging to audiobooks and extract the ids we need to delete
to_delete <- all_tracks %>% 
  filter(track.album.id %in% audiobooks_id)
table(to_delete$track.album.name) # quick check that the filter worked 
to_delete_ids <- to_delete$track.id # extract the ids
```

```{r}
# define function to delete ids (limited to 50 at a time) -> not part of spotifyr
# cf https://developer.spotify.com/documentation/web-api/reference/library/remove-tracks-user/
delete_ids <- function(ids) {
  httr::DELETE("https://api.spotify.com/v1/me/tracks", config = httr::config(token = spotifyr::get_spotify_authorization_code()),
             query = list(ids=paste0(ids, collapse = ",")))
}

# can only delete 50 at once, so split
del_groups <- split(to_delete_ids, ceiling(seq_along(to_delete_ids) / 50 )) # from stackoverflow 

# apply function
del_groups %>% 
  purrr::map(delete_ids)

```

